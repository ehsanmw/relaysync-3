#! /bin/bash

if [ "$HOSTNAME" == 'wu-relay0' ]; then
    # Don't run on the master copy
    exit 0
fi

source /root/.bashrc 2>/dev/null
source /root/relay-data.sh
source /opt/ozmt/zfs-tools-init.sh

if [ "$owncloud" == '' ]; then
    owncloud="nrg-owncloud.wustl.edu"
fi

pools=$(pools)

##
## Create .netrc
##

[ -f ~/.netrc ] && rm -f ~/.netrc
touch ~/.netrc
chmod 600 ~/.netrc
echo "machine $owncloud" > ~/.netrc
echo "login $oc_user" >> ~/.netrc
echo "password $oc_password" >> ~/.netrc

##
# Fix root emails
##
cd /etc/postfix
myorigin=`cat /etc/postfix/main.cf | grep myorigin | awk -F ' = ' '{print $2}'`
cat virtual |grep -q "${myorigin}"
if [ $? -ne 0 ]; then
    echo "root@${myorigin}  relay-ops@nrg.wustl.edu" > virtual
    postmap virtual
    systemctl restart postfix
fi
relayhost=`cat /etc/postfix/main.cf | grep relayhost |  awk -F ' = ' '{print $2}'`
if [ "$relayhost" != '' ]; then
    cat /etc/postfix/main.cf | grep -q debug_peer_list
    if [ $? -ne 0 ]; then
        cp main.cf main.cf.no_debug
        echo >> main.cf
        echo "debug_peer_list = $relayhost" >> main.cf
        systemctl restart postfix
    fi
fi      
cd

##
# Update scripts
##

su ${HOSTNAME}_sync -c "cd /raw/scripts;git pull" 1>/dev/null

##
# Clean up long running sync
##

find /tmp -mtime +1 -name mars-sync-running -exec echo "Killing mars-to-relay" \; -exec rm -f {} \; -exec killall mars-to-relay.sh \; 
find /tmp -mtime +1 -name raw-sync-running -exec echo "Killing relay-to-backup" \; -exec rm -f {} \; -exec killall relay-to-backup.sh \; 

##
# Flush owncloud folder
##

if [ -f /tmp/resyncing.owncloud ]; then
    # First time will take a while. Don't run more than one sync
    exit 0
fi

if [ ! -f /root/owncloud-resync-2017-08-23 ]; then
    touch /root/owncloud-resync-2017-08-23
    touch /tmp/resyncing.owncloud
    rm -rf /owncloud
    mkdir /owncloud
fi

##
## Collect log data
##

mkdir -p /owncloud/${HOSTNAME}/log/{system,xnat,raw}
mkdir -p /owncloud/${HOSTNAME}/stats

rsync -a /var/log/ /owncloud/${HOSTNAME}/log/system/
rsync -a --delete /xnat/logs/ /owncloud/${HOSTNAME}/log/xnat/
rsync -a /raw/data/logs/ /owncloud/${HOSTNAME}/log/raw/
rsync -a /etc/ /owncloud/${HOSTNAME}/etc/
rsync -a /root/.bashrc /owncloud/${HOSTNAME}/stats/root.bashrc.txt
cp -a /var/spool/cron/root /owncloud/${HOSTNAME}/cron.root.txt
cp -a /var/spool/cron/${HOSTNAME}_sync /owncloud/${HOSTNAME}/cron.${HOSTNAME}_sync.txt

##
## Collect stats
##

#added by Ehsan for temporary -- will remove later
if [ "$HOSTNAME" == 'MGH-relay1' ]; then
    date > /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    uptime >> /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    free >> /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    lsblk >> /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    traceroute -n asp-connect1.wustl.edu >> /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    ip a >> /tmp/troubleshooting;
    echo >> /tmp/troubleshooting;
    ascp -T -l 100m -P 33001 -i ~/.ssh/id_rsa /tmp/troubleshooting asp-connect1.wustl.edu:/tmp
    exit 0
fi

# Causing problems when collecting on localhost
# Unset and re-source .bashrc after stats collection
unset https_proxy
unset http_proxy
unset no_proxy

rm -rf /owncloud/${HOSTNAME}/stats

mkdir -p /owncloud/${HOSTNAME}/stats
/opt/puppetlabs/bin/facter -j > /owncloud/${HOSTNAME}/stats/facter.json
ifconfig > /owncloud/${HOSTNAME}/stats/ifconfig.txt
zfs list -t all > /owncloud/${HOSTNAME}/stats/zfs-list.txt
echo $'\n'"$(date)" >> /owncloud/${HOSTNAME}/stats/zfs-list.txt
uptime > /owncloud/${HOSTNAME}/stats/uptime.txt
systemctl > /owncloud/${HOSTNAME}/stats/systemctl.txt
set > /owncloud/${HOSTNAME}/stats/root_environment.txt
ls -lah /root > /owncloud/${HOSTNAME}/stats/root_ls-lha.txt
postqueue -p > /owncloud/${HOSTNAME}/stats/postqueue.txt
ls -1 /raw/data/staging > /owncloud/${HOSTNAME}/stats/relay-raw-data-staging_files.txt
ls -lh /tmp > /owncloud/${HOSTNAME}/stats/tmp-ls.txt
ls -lht /raw/data/staging > /owncloud/${HOSTNAME}/stats/raw-data-staging_files-lht.txt
echo $'\n'"$(date)" >> /owncloud/${HOSTNAME}/stats/raw-data-staging_files-lht.txt
rm -f /owncloud/${HOSTNAME}/stats/raw-data-staging_files-lht.text
ps awwwx > /owncloud/${HOSTNAME}/stats/ps-awwx.txt
cp /home/${HOSTNAME}_sync/.ssh/id_rsa.pub /owncloud/${HOSTNAME}/stats/id_rsa.pub
cp /home/${HOSTNAME}_sync/.hcpxnat_intradb.cfg /owncloud/${HOSTNAME}/stats/.hcpxnat_intradb.cfg.txt
cp /home/${HOSTNAME}_sync/.hcpxnat_relay.cfg /owncloud/${HOSTNAME}/stats/.hcpxnat_relay.cfg.txt
[ -f /tmp/ping_asp-connect.txt ] && cp /tmp/ping_asp-connect.txt /owncloud/${HOSTNAME}/stats/ping_asp-connect.txt
mkdir -p /owncloud/${HOSTNAME}/stats/scripts/
rsync -a /raw/scripts/ /owncloud/${HOSTNAME}/stats/scripts/
ls -lha /xnat/home/plugins > /owncloud/${HOSTNAME}/stats/xnat-home-plugins-lha.txt

if [ -f /raw/scripts/config.yaml ]; then
    source /raw/scripts/parse-config.sh
    eval $(parse_yaml /raw/scripts/config.yaml)
    ssh root@$mars_ip ls -1 /rawdata > /owncloud/${HOSTNAME}/stats/mars_raw_files.txt
    ssh root@$mars_ip ls -lht /rawdata > /owncloud/${HOSTNAME}/stats/mars_raw_files-lht.txt
    ssh root@$mars_ip df -h > /owncloud/${HOSTNAME}/stats/mars_du.txt
    echo $'\n'"$(date)" >> /owncloud/${HOSTNAME}/stats/mars_raw_files-lht.txt
    echo $'\n'"$(date)" >> /owncloud/${HOSTNAME}/stats/mars_du.txt
fi

alias "[auth]"=true
alias "[site]"=true
source /home/${HOSTNAME}_sync/.hcpxnat_relay.cfg 1>/dev/null 2>&1
curl -k -u ${username}:${password} https://localhost/data/experiments\?format\=csv\&columns\=label,date,insert_date > /owncloud/${HOSTNAME}/stats/xnat-sessions.csv 2> /owncloud/${HOSTNAME}/stats/xnat-sessions.csv.err.txt
curl -k -u ${username}:${password} https://localhost/data/experiments\?format=\json > /owncloud/${HOSTNAME}/stats/xnat-sessions.json 2> /owncloud/${HOSTNAME}/stats/xnat-sessions.json.err.txt
curl -k -u ${username}:${password} https://localhost/data/prearchive\?format=\json > /owncloud/${HOSTNAME}/stats/xnat-prearchive.json 2> /owncloud/${HOSTNAME}/stats/xnat-prearchive.json.err.txt
curl -k -u ${username}:${password} https://localhost/xapi/xsync/history > /owncloud/${HOSTNAME}/stats/xsync-history.json 2> /owncloud/${HOSTNAME}/stats/xsync-history.err.txt
curl -k -u ${username}:${password} https://localhost/xapi/xsync/progress/CCF_HCA_ITK > /owncloud/${HOSTNAME}/stats/xsync-progress.csv 2> /owncloud/${HOSTNAME}/stats/xsync-progress-HCA.err.txt
curl -k -u ${username}:${password} https://localhost/xapi/xsync/progress/CCF_HCD_ITK >> /owncloud/${HOSTNAME}/stats/xsync-progress.csv 2> /owncloud/${HOSTNAME}/stats/xsync-progress-HCD.err.txt
rm -f /owncloud/${HOSTNAME}/xnat-sessions.*

# Source bashrc again to get proxy settings back
source /root/.bashrc 2>/dev/null

##
## Use randomness to cause stagger between numerous relays
## 

if [ ! -t 1 ]; then
    sleep $(( $RANDOM % 120 ))
fi

##
## Sync Owncloud
##
sync_owncloud () {
    SECONDS=0
    if [ "$HOSTNAME" == 'harvard-relay1' ]; then
        proxy='--httpproxy https://rcproxy.rc.fas.harvard.edu:3128'
    fi
    if [ "$HOSTNAME" == 'BMC-relay1' ]; then
        tmout="18m"
    else
        tmout="10m"
    fi
    
    if [ -t 1 ]; then
        #allow long runtime on the terminal
        tmout='2h'
    fi
    
    if [ -f /tmp/resyncing.owncloud ]; then
        tmout='12h'
    fi

    #timeout $tmout owncloudcmd $proxy --non-interactive --silent -n /owncloud ownclouds://${owncloud}/owncloud/remote.php/webdav  1>/var/log/owncloud_sync.log  2>/var/log/owncloud_sync_error.log
    user=${HOSTNAME}_sync
    /root/.aspera/connect/bin/ascp -i /home/$user/.ssh/id_rsa -P 33001 -l 10G /owncloud/${HOSTNAME}/* $user@asp-connect1.wustl.edu:/data/relays/${HOSTNAME} 1>/var/log/ascp.log 2>/var/log/ascp.error.log

    result=$?
    case $result in
        '0')
            # Owncloud always returns 0 
            echo "$SECONDS seconds" > /owncloud/${HOSTNAME}/stats/owncloud-sync-time.txt
            cat /var/log/owncloud_sync_error.log | grep -q "ERROR"
            if [ $? -eq 0 ]; then
                cp /var/log/owncloud_sync_error.log /tmp/owncloud_error-${HOSTNAME}.txt
                error "${HOSTNAME}: Error syncing owncloud" /tmp/owncloud_error-${HOSTNAME}.txt
                cat /var/log/owncloud_sync_error.log
            fi
            ;;
        '124')
            echo "Timeout syncing owncloud."
            ;;
        *)
            #echo "Unknown error syncing owncloud $result"
            #cat /var/log/owncloud_sync_error.log
            ;;
    esac
}

sync_owncloud

rm -f /tmp/resyncing.owncloud

##
## Run additional scripts
##

if [ -f /owncloud/${HOSTNAME}/${HOSTNAME}-update.sh ]; then
    chmod +x /owncloud/${HOSTNAME}/${HOSTNAME}-update.sh
    timeout 10m /owncloud/${HOSTNAME}/${HOSTNAME}-update.sh
fi

if [ -f /owncloud/${HOSTNAME}/${HOSTNAME}-run-once.sh ]; then
    chmod +x /owncloud/${HOSTNAME}/${HOSTNAME}-run-once.sh
    timeout 10m /owncloud/${HOSTNAME}/${HOSTNAME}-run-once.sh
    mv /owncloud/${HOSTNAME}/${HOSTNAME}-run-once.sh /owncloud/${HOSTNAME}/${HOSTNAME}-run-once.sh_$(now_stamp)
    sync_owncloud
fi

if [ -f /owncloud/relay-common/relay-common-update.sh ]; then
    chmod +x /owncloud/relay-common/relay-common-update.sh
    cd /owncloud/relay-common/
    dos2unix relay-common-update.sh 2>/dev/null
    timeout 10m /owncloud/relay-common/relay-common-update.sh
fi

touch /owncloud/${HOSTNAME}/relay-common-run-once.sums

if [ -f /owncloud/relay-common/relay-common-run-once.sh ]; then
    sum=`md5sum /owncloud/relay-common/relay-common-run-once.sh | cut -f 1 -d ' ' `
    cat /owncloud/${HOSTNAME}/relay-common-run-once.sums | grep -q $sum
    if [ $? -ne 0 ]; then
        echo $sum >> /owncloud/${HOSTNAME}/relay-common-run-once.sums
        cp /owncloud/relay-common/relay-common-run-once.sh /owncloud/${HOSTNAME}/relay-common-run-once.sh      
        chmod +x /owncloud/${HOSTNAME}/relay-common-run-once.sh   
        timeout 1h /owncloud/${HOSTNAME}/relay-common-run-once.sh   
        sync_owncloud   
    fi
fi

# Add proxy to yum.conf
if [ "$https_proxy" != "" ]; then
    cat /etc/yum.conf | grep -q "proxy="
    if [ $? -ne 0 ]; then
        echo "proxy=${https_proxy}" >> /etc/yum.conf
    fi
fi

# Install packages

if [ ! -f /usr/bin/dos2unix ]; then
    timeout 2m yum -y install dos2unix || exit
fi

if [ ! -f /bin/nmap ]; then
    timeout 2m yum -y install nmap
fi

# Update crontab(s)

if [ -f /owncloud/${HOSTNAME}/cron.root.new.txt ]; then
     cd /owncloud/${HOSTNAME}
     dos2unix cron.root.new.txt && crontab /owncloud/${HOSTNAME}/cron.root.new.txt && rm -f /owncloud/${HOSTNAME}/cron.root.new.txt
fi

if [ -f /owncloud/${HOSTNAME}/cron.${HOSTNAME}_sync.new.txt ]; then
    cd /owncloud/${HOSTNAME} 
    dos2unix cron.${HOSTNAME}_sync.new.txt && crontab -u ${HOSTNAME}_sync /owncloud/${HOSTNAME}/cron.${HOSTNAME}_sync.new.txt && rm -f /owncloud/${HOSTNAME}/cron.${HOSTNAME}_sync.new.txt
fi

